# WE_DevOps

**First Task**
Create a centos7 container.
Install git
Clone this repository. hgfja
Run the hello_world.py script.




**Second Task**

This repo contains the scripts needed for the linuxjobber DevOps work experience tasks, which the trainees are required to run.
Start by creating your mysql container. Create a database named hello_world in the mysql container.

*  Run this command...  
*  Change username to your database username and password to the password
*  ALTER USER 'username' IDENTIFIED WITH mysql_native_password BY 'password';

Next, create another container from a centos7 image or use previous one.
Install mysql,git and other required dependencies in this container.
Clone this repository.
When all is done, open up the hello_mysql.py script. Add your database host, database name, username and password in the script

Save the script and run it.
You should get a success message.

